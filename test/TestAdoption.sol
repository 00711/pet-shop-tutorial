pragma solidity ^0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Adoption.sol";


contract TestAdoption {
    // The address of the adoption contract to be teset
    Adoption adoption = Adoption(DeployedAddresses.Adoption());

    // The id of the pet used for testing
    uint expectedPetId = 8;

    //The expected owner of the pet is this contract
    address expectedAdopter = address(this);

    // Test the adopt function
    function testUserCanAdoptPet() public {
        uint returnedId = adoption.adopt(expectedPetId);
        Assert.equal(returnedId, expectedPetId, "Adoption of the expected pet should match the returned");
    }

    // Testing retrieval of single pets owner
    function testGetAdopterAddressByPetId() public{
        address adopter = adoption.adopters(expectedPetId);
        Assert.equal(adopter, expectedAdopter, "Adopter of the expected pet should be this contract");
    }

    //Test retrieval of all pet adopters
    function testGetAdopterAddressByPetIdInArray() public{
        address[16] memory adopters = adoption.getAdopters();
        Assert.equal(adopters[expectedPetId], expectedAdopter, "Adopter of the expected pet should be this contract");
    }
}
