const Adoption = artifacts.require("Adoption");

contract("Adoption", (accounts) => {
  let adoption;
  let expectedAdopter;

  before(async () => {
    adoption = await Adoption.deployed();
  });

  describe("Adopting a pet and retrieving addresses", async () => {
    before("Adopt a pet using accounts[0]", async () => {
      await adoption.adopt(8, {from: accounts[0]});
      expectedAdopter = accounts[0];
    })

    it("Can fetch the address of an adopter by pet id", async() => {
      const adopter = await adoption.adopters(8);
      assert.equal(adopter, expectedAdopter, "The adopter of the pet should be this contract");
    })

    it("Can fetch the collection of all adopters addresses", async() => {
      const adopters = await adoption.getAdopters();
      assert.equal(adopters[8], expectedAdopter, "The adopter of the pet should be in the list");
    });
  })
});
